import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';
import { User } from 'src/app/interface/login';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private token = null;

  constructor(private http:HttpClient) { }

  postAuth(user:User):Observable<{token:string}> {
    return this.http.post<{token:string}>('https://dummyjson.com/auth/login', user)
    .pipe(
      tap(
        ({token})=>{
          localStorage.setItem('auth-token', token)
          this.setToken(token);
        }
      )
    )
  }
  setToken(token:any) {
    this.token = token;
  }
}