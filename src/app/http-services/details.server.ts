import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { Details } from 'src/app/interface/details-product';

@Injectable({
  providedIn: 'root'
})
export class DetailsService  {

  constructor(private http:HttpClient) { }

  public getDetail(id:any):Observable<Details>{
    return this.http.get<Details>('https://dummyjson.com/products/' + id)
  }
}
