import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http'
import { Subject, Observable, BehaviorSubject } from 'rxjs';
import { IProductResponse, IProduct } from 'src/app/interface/product';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  onSearch: any = new Subject();
  busket: BehaviorSubject<IProduct[]> = new BehaviorSubject<IProduct[]>([]);

  setBusket(data: IProduct[]) {
    this.busket.next(data);
    localStorage.setItem('busket', JSON.stringify(data));
  }



  constructor(
    private http: HttpClient,
  ) { }

  getProducts(categorie?: string, limit = 15, skip = 0,): Observable<IProductResponse> {
    const params = new HttpParams({ fromObject: { limit, skip } })
    return this.http.get<IProductResponse>('https://dummyjson.com/products' + (categorie ? ('/category/' + categorie) : ''), { params })
  }


  searchProducts(val: string): Observable<IProductResponse> {
    const params = new HttpParams({ fromObject: { q: val } })
    return this.http.get<IProductResponse>('https://dummyjson.com/products/search', { params })
  }

  busketProducts() {
    // localStorage.setItem('busket', JSON.stringify(busket));
    // let item = JSON.parse(localStorage.getItem('busket'));
  }
}
