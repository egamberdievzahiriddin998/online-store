import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  constructor(private http:HttpClient) { }

  getCatigorie():Observable<any> {
    return this.http.get<any>('https://dummyjson.com/products/categories')
  }
}
