import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AcauntComponent } from './acaunt.component';

describe('AcauntComponent', () => {
  let component: AcauntComponent;
  let fixture: ComponentFixture<AcauntComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AcauntComponent]
    });
    fixture = TestBed.createComponent(AcauntComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
