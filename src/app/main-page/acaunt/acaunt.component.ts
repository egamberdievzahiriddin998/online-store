import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DetailsService } from 'src/app/http-services/details.server';
import { UsersService } from 'src/app/http-services/users.service';
import { IUser } from 'src/app/interface/users';

@Component({
  selector: 'app-acaunt',
  templateUrl: './acaunt.component.html',
  styleUrls: ['./acaunt.component.scss']
})
export class AcauntComponent implements OnInit{
  user: IUser;
  constructor( 
    private users:UsersService
    ){}
    
  ngOnInit(): void {
    this.users.getUsers().subscribe((u: any) => {
      this.user  = u;
      console.log(this.user);
    })
  }
}
