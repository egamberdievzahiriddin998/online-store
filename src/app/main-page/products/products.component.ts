import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductsService } from 'src/app/http-services/products.service';
import { IProduct } from 'src/app/interface/product';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  search: any = '';
  pageSize = 10;
  categorie: any = null;
  skip: any;
  pageSizes = [10, 30, 50]
  products: IProduct[] = [];
  pages: number[] = [];
  currentActive = 1;
  busket: IProduct[] = [];

  constructor(
    private productService: ProductsService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.productService.busket.subscribe(res => {
      console.log(res);
      this.busket = res;
    })
    this.productService.onSearch.subscribe((val: string) => {
      this.search = val;
      console.log(val);
      this.searchProducts();
    })
    this.route.queryParams.subscribe(res => {
      this.pageSize = res['limit'] || 10;
      this.categorie = res['categorie'];
      this.skip = res['skip'];
      this.getProducts();
    })
  }

  searchProducts() {
    window.scroll(0, 0);
    this.productService.searchProducts(this.search).subscribe(data => {
      this.products = data.products;
      this.getPagination(data.total);
    })
  }

  getProducts() {
    window.scroll(0, 0);
    this.productService.getProducts(this.categorie, this.pageSize, this.skip).subscribe(data => {
      this.products = data.products;
      this.getPagination(data.total);
    })
  }

  inBusket(product: IProduct) {
    return this.busket.some(x => x.id === product.id)
  }

  getPagination(total: number) {
    const pageCount = Math.ceil(total / this.pageSize);
    this.pages = [];
    for (let i = 1; i <= pageCount; i++) {
      this.pages.push(i);
    }
  }

  setPage(page: number) {
    this.currentActive = page;
    const skip = (page - 1) * this.pageSize;
    this.router.navigate(
      [],
      {
        relativeTo: this.route,
        queryParams: { skip },
        queryParamsHandling: 'merge'
      });

  }

  onChange(event: any) {
    window.scroll(0, 0);
    console.log(event.target.value);
    this.router.navigate(
      [],
      {
        relativeTo: this.route,
        queryParams: { limit: event.target.value },
        queryParamsHandling: 'merge'
      });
  }

  prev() {
    this.currentActive--;
    if (this.currentActive > this.pages.length) {
      this.currentActive = this.pages.length;
    }
    const skip = (this.currentActive - 1) * this.pageSize;
    this.router.navigate(
      [],
      {
        relativeTo: this.route,
        queryParams: { skip },
        queryParamsHandling: 'merge'
      });
  }

  next() {
    this.currentActive++;
    if (this.currentActive > this.pages.length) {
      this.currentActive = this.pages.length;
    }
    const skip = (this.currentActive - 1) * this.pageSize;
    this.router.navigate(
      [],
      {
        relativeTo: this.route,
        queryParams: { skip },
        queryParamsHandling: 'merge'
      });
  }

  addToBusket(product: IProduct) {
    const index = this.busket.findIndex((x: any) => x.id === product.id);
    if (index < 0) {
      product.count = 1;
      this.busket.push(product);
      this.productService.setBusket(this.busket);
    }
  } 
}
