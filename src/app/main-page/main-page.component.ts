import { Component, OnInit } from '@angular/core';
import { CategoriesService } from '../http-services/catigories.service';
import { Router } from '@angular/router';
import { ProductsService } from '../http-services/products.service';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit {
  catigories: string[] = [];
  search:string="";
  catigorieActive = null;

  setActiveClass(index:any) {
    
  }

  constructor(
    private catigorieService: CategoriesService,
    private productsService: ProductsService,
    private router: Router
  ) { }

  ngOnInit(): void {
    const busket = JSON.parse(localStorage.getItem('busket') || '[]');
    this.productsService.busket.next(busket);
    this.getCaregories();
  }

  getCaregories() {
    this.catigorieService.getCatigorie().subscribe((categorie: any) => {
      this.catigories = categorie;
      console.log(this.catigories)
    })
  }

  filter(categorie: string, index:any) {
    this.router.navigate(['/main/product'], {queryParams: {categorie}});
    this.catigorieActive = index;
  }

  searchProducts(productName:string):void {
    this.search = productName;
  }
}
