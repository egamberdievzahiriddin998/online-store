import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/http-services/products.service';
import { IProduct } from 'src/app/interface/product';

@Component({
  selector: 'app-busket',
  templateUrl: './busket.component.html',
  styleUrls: ['./busket.component.scss']
})
export class BusketComponent implements OnInit {
  products: IProduct[] = [];
  productquantity: any = 1;
  constructor(
    private productService: ProductsService,
  ) { }

  ngOnInit(): void {
    this.productService.busket.subscribe(res => {
      this.products = res;
    })

  }

  deleteProduct(product: number) {
    this.products.splice(product, 1)
    this.productService.setBusket(this.products)
  }

  // quantity(value: string, product: IProduct) {
  //   product.count = '';
  //   if(this.productquantity > 1 && value == 'min') {
  //     this.productquantity--;
  //     product.count = this.productquantity;
  //   }else if(this.productquantity >= 1 && value == 'max') {
  //     this.productquantity++;
  //     product.count = this.productquantity;
  //   }
  //   console.log('dasdasdasd' + product.count)
  //   // this.productService.setBusket(product.count);
  //   // localStorage.setItem('busket', JSON.stringify(product.count));
  // }

  calculate(product: IProduct, num: number) {
    product.count += num;
    if (product.count < 1) {
      product.count = 1;
    }
    this.productService.setBusket(this.products);
  }
}
