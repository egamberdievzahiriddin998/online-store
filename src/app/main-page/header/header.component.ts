import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductsService } from 'src/app/http-services/products.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() buttonClick: boolean;
  search = '';
  productsLength = 0;
  constructor(private route: Router,
              private productsService: ProductsService) {

  }
  ngOnInit() {
    this.productsService.busket.subscribe(data => {
      this.productsLength = data.length;
    })
  }


  onSearch() {
    this.productsService.onSearch.next(this.search);
    this.search = '';
  }


  mainBasket() {
    this.route.navigate(['/main/busket'])
  }

  mainAcaunt() {
    this.route.navigate(['/main/acaunt'])
  }
}
