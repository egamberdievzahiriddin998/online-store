import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { ProductsComponent } from './products/products.component';
import { DetailComponent } from './detail/detail.component';
import { MainPageComponent } from './main-page.component';
import { MainPageRoutingModule } from './main-page-routing.module';
import { HeaderComponent } from './header/header.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BusketComponent } from './busket/busket.component';
import { AcauntComponent } from './acaunt/acaunt.component';



@NgModule({
  declarations: [
    MainPageComponent,
    ProductsComponent,
    DetailComponent,
    HeaderComponent,
    BusketComponent,
    AcauntComponent,
  ],
  imports: [
    CommonModule,
    MainPageRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class MainPageModule { }
