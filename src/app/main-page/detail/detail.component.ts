import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DetailsService } from 'src/app/http-services/details.server';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit{
  product:any;
  constructor(
    private activatedRouter:  ActivatedRoute,
    private detailsService: DetailsService,
    ){}
    
  ngOnInit(): void {
    window.scroll(0,0);
    let id = this.activatedRouter.snapshot.paramMap.get("id")
    if(id){
      this.detailsService.getDetail(id).subscribe((data)=>{
        this.product = data;
        console.log(this.product);
      })
    }
  }
}
