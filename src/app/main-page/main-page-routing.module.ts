import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductsComponent } from './products/products.component';
import { DetailComponent } from './detail/detail.component';
import { MainPageComponent } from './main-page.component';
import { BusketComponent } from './busket/busket.component';
import { AcauntComponent } from './acaunt/acaunt.component';

const routes: Routes = [
  {
    path: '',
    component: MainPageComponent,
    children: [
      { path: '', redirectTo: 'product', pathMatch: 'full' },
      { path: 'product', component: ProductsComponent },
      { path: 'product/:id', component: DetailComponent },
      { path: 'busket', component: BusketComponent },
      { path: 'acaunt', component: AcauntComponent },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainPageRoutingModule { }
